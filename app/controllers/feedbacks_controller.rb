class FeedbacksController < ApplicationController
  def new
  	@feedback = Feedback.new
  end

  def create
  	@feedback = Feedback.new params[feedback_params]
  	@feedback.save
      if @feedback.save
  		  redirect_to spree_url
  	end
  end

  def about
  end

  private

  def feedback_params
  	params.require(:feedback).permit(:title, :text, :mail)
  end
end
